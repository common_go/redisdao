package redisdao

import (
	"github.com/go-redis/redis/v8"
)

func NewSimpleRedis(instance string) *redis.Client {
	Init()
	return getInstance(instance)
}
