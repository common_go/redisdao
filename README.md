# udcredis
udcredis 基于 `github.com/go-redis/redis` 封装，将 `redis` 实例工厂化，便于接入项目使用。


## 安装
```shell script
go get gitee.com/common_go/redisdao
```

## 配置
```ini
[Redis]
redis = localhost:6379
delayRedis = localhost:16379

[RedisConfig]
redis.password =
redis.idletimeout = 240
redis.poolsize = 100
redis.db = 0
delayRedis.password = AABBCC+
delayRedis.idletimeout = 240
delayRedis.poolsize = 100
delayRedis.db = 7
```

## 使用示例
```go
import (
    "context"
    "github.com/go-redis/redis/v8"

    "gitee.com/common_go/redisdao"
)

var ctx = context.Background()

func ExampleClient() {
    rdb := redisdao.NewSimpleRedis("redis")

    err := rdb.Set(ctx, "key", "value", 0).Err()
    if err != nil {
        panic(err)
    }

    val, err := rdb.Get(ctx, "key").Result()
    if err != nil {
        panic(err)
    }
    fmt.Println("key", val)

    val2, err := rdb.Get(ctx, "key2").Result()
    if err == redis.Nil {
        fmt.Println("key2 does not exist")
    } else if err != nil {
        panic(err)
    } else {
        fmt.Println("key2", val2)
    }
}
```

## redis 操作文档
https://redis.uptrace.dev/